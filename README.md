# AI via Planning + LLMs
## Aims of the project
Creating a friendly AI capable of reliable reasoning.

Every LLM in existence is a blackbox, and alignment relying on tuning the blackbox never succeeds - that is evident by that fact that even models like ChatGPT get jailbroken constantly. There will always be a new jailbreak. Thus, these models are inherently unsafe. This is an attempt at alternative architecture.

## Approach
We will use a small LLM to parse user query as STRIPS domain+problem. We will then use constraint solver to either produce a plan for the problem, or to conclude that no plan is possible, and the answer to query is "no". Constraint solver will include alignment constraints, and the entire plan will be trivially interpretable. We will then feed the plan back to a small model to execute the actions.


## References
[LLM+P](https://arxiv.org/abs/2304.11477)

[On the Role of Large Language Models in Planning](https://www.youtube.com/watch?v=KTagKkWT2n4&list=PLNONVE5W8PCR5HR1vp4t2TDnBxGTIJUcW&index=1)
# Overall architecture
The project will be structured as a straightforward pipeline of the following components:

1. User prompt

A natural language user prompt.

2. Parser.

We will use [parser llm] to parse every word within the user prompt, unless it is already recorded within [vector database], within the user prompt into [planner format] separately.

3. Caching Database.

[vector database] will store word-[planner format] definition pairs and retrieve them with some meaning similarity threshold.

4. Problem compiler

We will use [problem compiler] to translate the user prompt into a formal [planner format] problem.

5. Domain compiled

We will use [domain compiler] to populate a planning domain with relevant facts in [planner format].

6. Planner

We will use [planner] to attempt to create a plan or prove that no plan exists.

7. Alignment validator

We will use [alignment validator] to verify that the plan is not harmful and does not violate constraints.

8. Action dispatcher

We will use action dispatcher to satisfy user query by either printing the answer or performing required actions, depending on prompt modality(hypothetical/agentic). Hypothetical query refers to a question, and action query to a request.


# Components currently used
[planner format] STRIPS for now. STRIPS does not allow for multi-agent solutions and must be replaced with a more advanced format.

[parser llm] GPT4 api for now. Must be replaced with the smallest viable LLM, fine-tuned for producing [planner format]

[vector database](https://github.com/google/leveldb) simple string-string cache for now. Must be replaced with an actual vector database and a decision on how semantic similarity is judged must be made.

[problem compiler] GPT4 api for now, we will simply ask GPT4 to write the problem from the prompt. The question of translating from prompt to problem must be examined, and actual compiler implemented.

[domain compiler] For now we will simply populate the domain with all the definitions that we have in [vector database]. Actual compiler must be designed, otherwise planning problem computational complexity quickly becomes unfeasible. An possible approach to this would be to use vector search to iteratively dynamically populate the planning domain with facts relevant to the prompt and attempting to solve the problem, with similarity constraint relaxing if solution cannot be reached.

[planner](https://www.fast-downward.org) seems like the best option. Planner is the key component of the system, and shall have to be continually reexamined and perhaps a new one would have to be engineered or an existing one extended.

[alignment validator] For now we will not do any validation. Validation shall have to be implemented in the future, first as simple sentiment analysis using BERT or similar, and discarding any plan that does not satisfy alignment constraints. Afterwards, the issue, being the second keystone of the project, shall have to be examined extensively. Validation layer should then ideally be integrated into the [planner], to impose alignment constraints early during search.

[Action dispatcher] For now we will simply print the plan as proposed. Actual implementation will likely consist of a set of minimal viable LLMs, fine-tuned to carry out specific interactions with interfaces such as computer screen and virtual keyboard, and another minimal viable LLM that will translate steps of the plan into action commands for them.